# Big Data BPMN Mediator
A mediator architecture to work with BPMN XML-applications and convert them to other representations. Accessible through a REST interface. Allows to store and retrieve diagrams from the servers file-system.

## How to build and run
This is a maven based spring boot application. You need a system with maven and a JDK 11 to run or build this application.  
**Build:** Simply `mvn install` in the root directory  
**Run:** Build and switch to the target directory. Then type `java -jar [nameOfTheActualJar].jar`

## Enpoint BPMN
[host]/endpoints/bpmn

## Enpoint hPDL
[host]/endpoints/hpdl

### POST
**URL Path:** /[filename]  
**Applies to:** bpmn  
**Headers:** Content-Type: text/plain  
**Body:** Diagram to store  
Creates a diagram in the servers file-system. Returns TRUE in case of success and FALSE otherwise.

### GET
**URL Path:** /  
**Applies to:** bpmn + hpdl  
Returns the file-names of all diagrams stored on the server as JSON array.

### GET
**URL Path:** /[filename]  
**Applies to:** bpmn + hpdl  
Returns the content of specified diagramm as raw text.

### PUT
**URL Path:** /[filename]  
**Applies to:** bpmn  
**Headers:** Content-Type: text/plain  
**Body:** New diagram content  
Replaces the content of specified diagramm withe the given one. Returns TRUE in case of success and FALSE otherwise.

### DELETE
**URL Path:** /[filename]  
**Applies to:** bpmn  
Deletes specified diagram from the servers file-system. Returns TRUE in case of success and FALSE otherwise.
