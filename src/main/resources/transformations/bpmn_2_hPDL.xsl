<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet version="1.0" 
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:bpmn="http://www.omg.org/spec/BPMN/20100524/MODEL" 
	xmlns:camunda="http://camunda.org/schema/1.0/bpmn"
	xmlns="uri:oozie:workflow:1.0">

	<xsl:output method="xml"/>

	<xsl:template match="bpmn:process">
		<workflow-app name="{@id}" 
			xmlns="uri:oozie:workflow:1.0">
			<xsl:apply-templates/>
		</workflow-app>
	</xsl:template>

	<!-- Template for "start" -->
	<xsl:template match="bpmn:startEvent">
		<xsl:variable name="flowID" select="bpmn:outgoing" />
		<xsl:variable name="flowSource" select="//bpmn:sequenceFlow[@id=$flowID]/@sourceRef" />
		<xsl:variable name="flowTarget" select="//bpmn:sequenceFlow[@id=$flowID]/@targetRef" />

		<start to="{$flowTarget}"/>
	</xsl:template>

	<!-- Template for join- and fork-nodes -->
	<xsl:template match="bpmn:parallelGateway">
		<xsl:choose>

			<!-- Case: Fork -->
			<xsl:when test="bpmn:extensionElements/camunda:properties/camunda:property[@name='type'][@value='fork']">
				<fork name="{@id}">
					<xsl:for-each select="bpmn:outgoing">
						<xsl:variable name="flowID" select="text()" />
						<xsl:variable name="flowTarget" select="//bpmn:sequenceFlow[@id=$flowID]/@targetRef" />
						<path start="{$flowTarget}"/>
					</xsl:for-each>
				</fork>
			</xsl:when>

			<!-- Case: Join -->
			<xsl:when test="bpmn:extensionElements/camunda:properties/camunda:property[@name='type'][@value='join']">
				<xsl:variable name="flowID" select="bpmn:outgoing" />
				<xsl:variable name="flowTarget" select="//bpmn:sequenceFlow[@id=$flowID]/@targetRef" />
				<join name="{@id}" to="{$flowTarget}" />
			</xsl:when>
		</xsl:choose>
	</xsl:template>

	<!-- Template for "action" -->
	<xsl:template match="bpmn:task">
		<action name="{@id}">

			<!-- Build action type specific block -->
			<xsl:variable name="actionType" select="bpmn:extensionElements/camunda:properties/camunda:property[@name='type']/@value" />
			<xsl:choose>

				<!-- Action-type: Map-Reduce -->
				<xsl:when test="$actionType = 'map-reduce'">
					<map-reduce>
						<xsl:for-each select="bpmn:extensionElements/camunda:inputOutput/camunda:inputParameter">
							<xsl:choose>
								<xsl:when test="@name = 'configuration'">
									<xsl:element name="{@name}">
										<xsl:for-each select="camunda:map/camunda:entry">
											<xsl:element name="{@key}"><xsl:value-of select="text()" /></xsl:element>
										</xsl:for-each>
									</xsl:element>
								</xsl:when>

								<xsl:otherwise>
									<xsl:element name="{@name}"><xsl:value-of select="text()" /></xsl:element>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</map-reduce>
				</xsl:when>

				<!-- Action-type: Java -->
				<xsl:when test="$actionType = 'java'">
					<java>
						<xsl:for-each select="bpmn:extensionElements/camunda:inputOutput/camunda:inputParameter">
							<xsl:choose>
								<xsl:when test="@name = 'configuration'">
									<xsl:element name="{@name}">
										<xsl:for-each select="camunda:map/camunda:entry">
											<xsl:element name="{@key}"><xsl:value-of select="text()" /></xsl:element>
										</xsl:for-each>
									</xsl:element>
								</xsl:when>
								<xsl:when test="@name = 'prepare'">
									<xsl:element name="{@name}">
										<xsl:for-each select="camunda:map/camunda:entry">
											<xsl:element name="{@key}"><xsl:value-of select="text()" /></xsl:element>
										</xsl:for-each>
									</xsl:element>
								</xsl:when>

								<xsl:otherwise>
									<xsl:element name="{@name}"><xsl:value-of select="text()" /></xsl:element>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:for-each>
					</java>
				</xsl:when>

				<!-- All other types are not supported right now -->
				<xsl:otherwise></xsl:otherwise>
			</xsl:choose>

			<!-- Set outputs -->
			<xsl:for-each select="bpmn:outgoing">
				<xsl:variable name="flowID" select="text()" />
				<xsl:variable name="flowTarget" select="//bpmn:sequenceFlow[@id=$flowID]/@targetRef" />
				<xsl:variable name="flowType" select="//bpmn:sequenceFlow[@id=$flowID]/bpmn:extensionElements/camunda:properties/camunda:property[@name='type']/@value" />

				<xsl:choose>

					<!-- ERROR output -->
					<xsl:when test="$flowType = 'error'">
						<error to="{$flowTarget}"/>
					</xsl:when>

					<!-- OK output (default) -->
					<xsl:otherwise>
						<ok to="{$flowTarget}"/>
					</xsl:otherwise>
				</xsl:choose>
			</xsl:for-each>
		</action>
	</xsl:template>

	<!-- Template for "end" + "kill" -->
	<xsl:template match="bpmn:endEvent">
		<xsl:choose>

			<!-- Case: Kill (error) -->
			<xsl:when test="bpmn:extensionElements/camunda:properties/camunda:property[@name='type'][@value='kill']">
				<kill name="{@id}">
					<xsl:if test="bpmn:extensionElements/camunda:properties/camunda:property[@name='message']">
						<message>
							<xsl:value-of select="bpmn:extensionElements/camunda:properties/camunda:property[@name='message']/@value"/>
						</message>
					</xsl:if>
				</kill>
			</xsl:when>

			<!-- Case: End (finished) -->
			<xsl:otherwise>
				<end name="{@id}"/>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>


	<!-- Don't output anything for unrecognized nodes -->
	<xsl:template match="text()" />


</xsl:stylesheet>