package de.dberning.bdbm.server;

/**
 * Interface for writing related operations.
 */
public interface IWriteOperations {

	/**
	 * Creates a new file in the configured backend and returns TRUE
	 * when everything went right; FALSE otherwise.
	 * @return Content of the created diagram file
	 */
	public String create(String fileName, String diagram);

	/**
	 * Modifies an existing file in the configured backend and returns TRUE
	 * when everything went right; FALSE otherwise.
	 * @return Content of the updated diagram file
	 */
	public String update(String fileName, String diagram);

	/**
	 * Deletes an existing file in the configured backend and returns TRUE
	 * when everything went right; FALSE otherwise.
	 * @return TRUE for successful deletion; FALSE for error
	 */
	public Boolean delete(String fileName);
}