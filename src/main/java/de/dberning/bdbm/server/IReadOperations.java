package de.dberning.bdbm.server;

import java.util.List;

/**
 * Interface for read related methods.
 */
public interface IReadOperations {

	/**
	 * Returns a directory listing.
	 * @return A list of file names
	 */
	public List<String> read();

	/**
	 * Returns an BPMN XML-application as raw String.
	 * @return BPMN XML-application
	 */
	public String read(String fileName);
}