package de.dberning.bdbm.server;

/**
 * Interface for all server operations.
 */
public interface IServerOperations extends IReadOperations, IWriteOperations {}