package de.dberning.bdbm.server.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import de.dberning.bdbm.client.IClientOperations;
import de.dberning.bdbm.server.IReadOperations;
import de.dberning.bdbm.server.IWriteOperations;

/**
 * Server for communication with clients that use the BPMN
 * (http://www.omg.org/spec/BPMN/20100524/MODEL) XML-appliaction as exchange
 * format.
 */
@CrossOrigin(origins = {"http://studev2.fernuni-hagen.de:14083","http://localhost:7700"})
@RestController
@RequestMapping("/endpoints/bpmn")
public class BPMNServer implements IReadOperations, IWriteOperations {

	@Autowired
	private IClientOperations backendClient;

	@Override
	@RequestMapping(
		method = RequestMethod.POST,
		value = "/{fileName}", 
		produces = {"application/JSON"},
		consumes = {"text/plain"}
	)
	public String create(@PathVariable String fileName, @RequestBody String diagram) {
		return backendClient.create(fileName, diagram);
	}

	@Override
	@RequestMapping(
		method = RequestMethod.GET,
		value = "/", 
		produces = {"application/JSON"}
	)
	public List<String> read() {
		return backendClient.read();
	}

	@Override
	@RequestMapping(
		method = RequestMethod.GET,
		value = "/{fileName}", 
		produces = {"application/XML"}
	)
	public String read(@PathVariable String fileName) {
		return backendClient.read(fileName);
	}

	@Override
	@RequestMapping(
		method = RequestMethod.PUT,
		value = "/{fileName}", 
		produces = {"application/JSON"},
		consumes = {"text/plain"}
	)
	public String update(@PathVariable String fileName, @RequestBody String diagram) {
		return backendClient.update(fileName, diagram);
	}

	@Override
	@RequestMapping(
		method = RequestMethod.DELETE,
		value = "/{fileName}", 
		produces = {"application/JSON"}
	)
	public Boolean delete(@PathVariable String fileName) {
		return backendClient.delete(fileName);
	}
}