package de.dberning.bdbm.server.impl;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.List;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import de.dberning.bdbm.client.IClientOperations;
import de.dberning.bdbm.server.IReadOperations;

/**
 * Server for communication with clients that use the hPDL
 * (http://www.omg.org/spec/BPMN/20100524/MODEL) XML-appliaction as exchange
 * format.
 */

@CrossOrigin(origins = {"http://studev2.fernuni-hagen.de:14083","http://localhost:7700"})
@RestController
@RequestMapping("/endpoints/hpdl")
@Component
public class HPDLServer implements IReadOperations {

	@Autowired
	private IClientOperations backendClient;
	private TransformerFactory transformerFactory;
	Transformer fromMetaTransformer;

	public HPDLServer() {
		super();

		try {
			var fromMetaStylesheetURI = HPDLServer.class.getResourceAsStream("/transformations/bpmn_2_hPDL.xsl");
			var fromMetaStylesheetSource = new StreamSource(fromMetaStylesheetURI);
			transformerFactory = TransformerFactory.newInstance();
			fromMetaTransformer = transformerFactory.newTransformer(fromMetaStylesheetSource);

			// A failure at this stage can't be mitigated
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Converts from the meta-model (bpmn) to hPDL.
	 * 
	 * @param sourceObject Source diagram to convert
	 */
	private String convertFromMetaModel(String sourceObject) {
		if (sourceObject == null) {
			return null;
		}

		// Wrap source and target
		StreamSource sourceStream = new StreamSource(new StringReader(sourceObject));
		Writer resultWriter = new StringWriter();
		StreamResult resultSteam = new StreamResult(resultWriter);

		// Do the actual transformation
		try {
			fromMetaTransformer.transform(sourceStream, resultSteam);
			resultWriter.close();

		} catch (TransformerException | IOException e) {
			e.printStackTrace();
			return "";
		}

		// Returns the written stream result
		return resultWriter.toString();
	}

	@Override
	@RequestMapping(
		method = RequestMethod.GET,
		value = "/", 
		produces = {"application/JSON"}
	)
	public List<String> read() {
		return backendClient.read();
	}

	@Override
	@CrossOrigin(origins = "*")
	@RequestMapping(
		method = RequestMethod.GET,
		value = "/{fileName}", 
		produces = {"application/XML"}
	)
	public String read(@PathVariable String fileName) {
		return convertFromMetaModel(backendClient.read(fileName));
	}
}