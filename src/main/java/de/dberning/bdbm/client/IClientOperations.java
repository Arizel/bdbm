package de.dberning.bdbm.client;

import de.dberning.bdbm.server.IServerOperations;

/**
 * Interface for client specific operations.
 */
public interface IClientOperations extends IServerOperations {

	// Right now, client and server implement the same operations
}