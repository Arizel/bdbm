package de.dberning.bdbm.client.impl;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import de.dberning.bdbm.client.IClientOperations;

/**
 * Storage client utilizing the servers local file system.
 */
@Component
public class FilesystemClient implements IClientOperations {
	static private String SEP = FileSystems.getDefault().getSeparator();
	private Path diagramFolder;

	public FilesystemClient() {

		// Set diagrams folder. Create it if neccessary.
		diagramFolder = Paths.get(System.getProperty("user.dir") + SEP + "diagrams");
		if (!diagramFolder.toFile().isDirectory()) {
			try {
				Files.createDirectory(diagramFolder);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public String create(String fileName, String diagram) {
		try {
			Files.write(Paths.get(diagramFolder + SEP + fileName), diagram.getBytes());
		} catch (IOException e) {
			return null;
		}

		return diagram;
	}

	@Override
	public List<String> read() {
		List<String> result = new ArrayList<>();

		// Read all files from diagrams directory
		try (DirectoryStream<Path> stream = Files.newDirectoryStream(diagramFolder)) {
			for (Path file: stream) {
					result.add(file.getFileName().toString());
			}
		} catch (Exception e) {}
		
		return result;
	}

	@Override
	public String read(String fileName) {
		String result = null;

		try {
			result = new String(Files.readAllBytes(Paths.get(diagramFolder + SEP + fileName)));
		} catch (IOException e) {}

		return result;
	}

	@Override
	public String update(String fileName, String diagram) {

		// Files.write used in create does overwrite file contents in default configuration
		// which is what we want for a full replace, too.
		return create(fileName, diagram);
	}

	@Override
	public Boolean delete(String fileName) {
		try {
			Files.delete(Paths.get(diagramFolder + SEP + fileName));
		} catch (IOException e) {
			return false;
		}
		return true;
	}
}