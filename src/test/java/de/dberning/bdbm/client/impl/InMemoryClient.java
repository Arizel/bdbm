package de.dberning.bdbm.client.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.dberning.bdbm.client.IClientOperations;

/**
 * In-memory client for testing purposes
 */
public class InMemoryClient implements IClientOperations {
	private Map<String, String> files;

	public InMemoryClient() {
		files = new HashMap<>();
	}

	@Override
	public String create(String fileName, String diagram) {
		files.put(fileName, diagram);
		return diagram;
	}

	@Override
	public List<String> read() {
		return new ArrayList<String>(files.keySet());
	}

	@Override
	public String read(String fileName) {
		return files.get(fileName);
	}

	@Override
	public String update(String fileName, String diagram) {
		return create(fileName, diagram);
	}

	@Override
	public Boolean delete(String fileName) {
		return files.remove(fileName) != null ? true : false;
	}
}