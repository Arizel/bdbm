package de.dberning.bdbm.client.impl;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import org.apache.commons.lang3.RandomStringUtils;

@SpringBootTest
public class FilesystemClientTest {

	@Autowired
	private FilesystemClient filesystemClient;
	private String testFileName;
	private String testFileContent;

	/**
	 * Instantiate the test case.
	 * @param testName Name of the test case
	 */
	public FilesystemClientTest() {
		testFileName = RandomStringUtils.randomAlphabetic(8);
		testFileContent = "<bpmn/>";
	}

	/**
	 * Make sure an eventually created test file gets deleted
	 */
	@AfterEach
	public void tearDown() {
		filesystemClient.delete(testFileName);
	}
	
	/**
	 * Test case for the "create" method.
	 */
	@Test
	public void testCreate() {
		assertTrue(filesystemClient.create(testFileName, testFileContent).length() > 0);
	}

	/**
	 * Test case for the "read" method (directory).
	 */
	@Test
	public void testReadDirectory() {
		List<String> result = filesystemClient.read();
		assertNotNull(result);
	}

	/**
	 * Test case for the "read" method (file).
	 */
	@Test
	public void testReadFile() {
		filesystemClient.create(testFileName, testFileContent);
		String result = filesystemClient.read(testFileName);

		assertEquals(testFileContent, result);
	}

	/**
	 * Test case for the "update" method.
	 */
	@Test
	public void testUpdate() {
		filesystemClient.create(testFileName, testFileContent);
		assertEquals(testFileContent, filesystemClient.read(testFileName));
		
		filesystemClient.update(testFileName, "<xml/>");
		assertEquals("<xml/>", filesystemClient.read(testFileName));
	}

	/**
	 * Test case for the "delete" method
	 */
	@Test
	public void testDelete() {
		assertNull(filesystemClient.read(testFileName));

		filesystemClient.create(testFileName, testFileContent);
		assertNotNull(filesystemClient.read(testFileName));

		filesystemClient.delete(testFileName);
		assertNull(filesystemClient.read(testFileName));
	}
}