package de.dberning.bdbm.server.impl;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.net.URI;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

import de.dberning.bdbm.client.IClientOperations;
import de.dberning.bdbm.client.impl.InMemoryClient;
import de.dberning.bdbm.server.impl.HPDLServer;

@SpringBootTest
public class HPDLServerTest {
	private URI bpmnSampleURI;
	private File bpmnSampleFile;
	private String bpmnSampleFileName;
	private String bpmnSampleString;
	private IClientOperations inMemoryClient;

	@Autowired
	private HPDLServer hpdlServer;

	public HPDLServerTest() {
		try {
			bpmnSampleFileName = "beispiel_workflow.bpmn";
			bpmnSampleURI = ClassLoader.getSystemClassLoader().getResource(bpmnSampleFileName).toURI();
			bpmnSampleFile = new File(bpmnSampleURI);
			bpmnSampleString = FileUtils.readFileToString(bpmnSampleFile);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@BeforeEach
	public void setUp() {

		// Overwrite FilesystemClient with an in-memory implementation for testing
		inMemoryClient = new InMemoryClient();
		inMemoryClient.create(bpmnSampleFileName, bpmnSampleString);
		ReflectionTestUtils.setField(hpdlServer, "backendClient", inMemoryClient, IClientOperations.class);
	}

	/**
	 * Test case for the "read" method (directory).
	 */
	@Test
	public void testReadDirectory() {
		var result = hpdlServer.read();
		assertNotNull(result);
	}

	/**
	 * Test case for the "read" method (file).
	 */
	@Test
	public void testReadFile() {
		inMemoryClient.create(bpmnSampleFileName, bpmnSampleString);
		var result = hpdlServer.read(bpmnSampleFileName);
		assertNotNull(result);
	}
}