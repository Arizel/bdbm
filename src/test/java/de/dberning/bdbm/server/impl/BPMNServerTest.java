package de.dberning.bdbm.server.impl;

import static org.junit.jupiter.api.Assertions.*;

import java.io.File;
import java.net.URI;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.util.ReflectionTestUtils;

import de.dberning.bdbm.client.IClientOperations;
import de.dberning.bdbm.client.impl.InMemoryClient;
import de.dberning.bdbm.server.impl.BPMNServer;

@SpringBootTest
public class BPMNServerTest {
	private URI bpmnSampleURI;
	private File bpmnSampleFile;
	private String bpmnSampleFileName;
	private String bpmnSampleString;

	@Autowired
	private BPMNServer bpmnServer;

	public BPMNServerTest() {
		try {
			bpmnSampleFileName = "beispiel_workflow.bpmn";
			bpmnSampleURI = ClassLoader.getSystemClassLoader().getResource(bpmnSampleFileName).toURI();
			bpmnSampleFile = new File(bpmnSampleURI);
			bpmnSampleString = FileUtils.readFileToString(bpmnSampleFile);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@BeforeEach
	public void setUp() {

		// Overwrite FilesystemClient with an in-memory implementation for testing
		var inMemoryClient = new InMemoryClient();
		ReflectionTestUtils.setField(bpmnServer, "backendClient", inMemoryClient, IClientOperations.class);
	}

	/**
	 * Test case for the "create" method.
	 */
	@Test
	public void testCreate() {
		var result = bpmnServer.create(bpmnSampleFileName, bpmnSampleString);
		assertNotNull(result);
	}

	/**
	 * Test case for the "read" method (directory).
	 */
	@Test
	public void testReadDirectory() {
		var result = bpmnServer.read();
		assertNotNull(result);
	}

	/**
	 * Test case for the "read" method (file).
	 */
	@Test
	public void testReadFile() {
		bpmnServer.create(bpmnSampleFileName, bpmnSampleString);
		assertNotNull(bpmnServer.read(bpmnSampleFileName));
	}

	/**
	 * Test case for the "update" method.
	 */
	@Test
	public void testUpdate() {
		bpmnServer.create(bpmnSampleFileName, bpmnSampleString);
		var resultUpdated = bpmnServer.update(bpmnSampleFileName, "<bpmn/>");
		assertEquals("<bpmn/>", resultUpdated);
	}

	/**
	 * Test case for the "delete" method
	 */
	@Test
	public void testDelete() {
		bpmnServer.create(bpmnSampleFileName, bpmnSampleString);
		
		assertTrue(bpmnServer.delete(bpmnSampleFileName));
		assertNull(bpmnServer.read(bpmnSampleFileName));
	}
}